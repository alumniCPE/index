# CPE-ALUMNI (index file) README #

This repository contains configuration file from Project **ALUMNI CPE**

**app, public, index** repository is required

### File Directory ###

- **.htaccess** - _apache configuration file_ 
- **.htrouter.php** - _phalcon router file_
- **composer.bat** - _quick batch file to add 'composer'_
- **index.html** - _test page for phalcon_

*public and source files are contains in another repository*

### Installation ###
- delete **.git** file ( or type `rm -rf .git` in command line )
- create folder to contains web **assets** and **source** files
- download **app** repository into project folder
- download **public** repository into the project folder
- download **index** repository into the project folder then move files outside from index folder

### File Structure ###
    cpealumni
        |_ app
        |_ public
        |_ .htaccess
        |_ .htrouter.php
        |_ composer.bat
        |_ index.html
